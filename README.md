# MakerSpec Arduino Board Files

## **Installation:** 
After downloading and extracting these files copy the makerspec folder to your Arduino IDE hardware folder. This folder can generally be found in the same  
location as your sketchbook, however if you haven't installed any additional boards to the IDE yet, you may need to create a hardware folder manually.   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `Example Windows path: C:/Documents/Arduino/hardware`   
Once the makerspec folder is copied to your hardware folder you can launch Arduino IDE and should find the boards under Tools>Boards>MakerSpec. Select your board
(328PBnJ has a little bee silkscreened on the front with the words "Rev B", TinyTFT is a small white board with a Type C USB connector, and 328PTFT doesn't have a
bee silkscreened on front) and with the board connected select the correct COM port then you should be set to flash a sketch over USB!

## Using the supplied libraries:
Most of the libraries you will need to get started with the example programs are contained within this folder. All of these libraries were  written by someone
else and belong to their respective owners, though some of them have been modified by me while writing the examples, so in some  cases it's better to use these
rather than the original versions.


## **General Things to know:**
- On all of my boards the display backlight is connected to pin D5(or TFT_BL), this is a PWM capable pin so the backlight is fully adjustable programatically. 
- The display is also always connected to the typical SPI pins as well as: D10 for CS, D9 for DC, and D8 for RST. 
- All of the examples I have for these boards use a modified TFT library, so you may need to move the stock TFT library that comes with the Arduino IDE in order   
for it to work correctly. This library is usually found in the libraries folder wherever Arduino was installed   
(e.g. C:/Program Files/Arduino/libraries/TFT). If this library isn't moved or you don't specify a path to the   
correct TFT library supplied with these board files then it may give several errors while compiling. 
- The MicroSD card slot chip select(or CS ) pin is A0, which has also been defined as SD_CS in the board files.

## **Things to know about the TinyTFT board:**
    
- The TFT display is a 0.96" ST7735 65k Color SPI display that is 160x80 pixels
- In order to setup the supplied TFT library to work with the different dimensions of this display this line must be present in your sketches:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   `tft.initR(INITR_MINI160x80);`  
- like the other boards this boards display backlight is controlled via pin D5. 
- Unlike the other boards, because of its size this board lacks most protection, so it is important to connect power with the correct polarity and not try to charge   
any batteries off this board
- This board also has an NTC thermistor(on pin A6) with a 10k resistor for basic temperature monitoring. 
- The USB Type C port works in legacy mode and still uses the standard D+ D- USB lines and a regular USB-UART converter rather than the   
functionality added to the Type C port. In order to use the supplied library with   


## **Things to know about the 328PTFT**
- The 328PTFT is basically an arduino Nano mixed with a TFT screen, 5 way thumb stick and two tactile switches, MicroSD slot, an IO exapnder, and 5v and 3.3v power. 
- The 328PTFT doesn't have an on board NRF24l01 Transceiver so the supplied breakout board should have one on it
- The onboard USB single cell lithium ion battery charger is set to charge at about 800mAh
- There is built in reverse polarity protection, though I wouldn't advise testing it; I have, and your board should be safe if you connect a battery the wrong way!
- These boards have both a 5v boost and 3.3v LDO circuit so they can run off the full range of a lithium ion cell, or 5v USB, and provide both 5 and 3.3v on the   
corresponding pins. 

## **Things to know about the 328PBnJ:**
- The microcontroller on these boards is a newer, slightly more advanced version of the 328P on my other boards.   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [ATMega328P vs ATMega328PB](http://ww1.microchip.com/downloads/en/Appnotes/Atmel-42559-Differences-between-ATmega328P-and-ATmega328PB_ApplicationNote_AT15007.pdf)
- The TFT display on this board is a 1.8" ST7735 65k Color SPI display that is 128x160 pixels.
- This board has a few solder jumpers on the face of the board between the 5 Way thumbstick and tactile switches:   
    1. Right jumper connects or disconnects D4 to the PCF8574 interrupt pin
    2. Upper left is used to remove power to the NRF24l01 for low power projects
    3. Lower left is used to connect D3 to the NRF24l01 IRQ pin
- The NRF14l01 also uses SPI and has the CSN pin connected to D7 and CE pin connected to D6.
- The PCF8574 I2C IO expander is used to interface the 5 way thumbstick and two tactile switches with the microcontroller without eating up a bunch of IO.
- There is also an LED connected to P7 of the PCF8574. 
- The I2C address of the PCF8574 is set as 0x20 which is also noted next to the IC on the back of the board. 
- The onboard LDR is connected to pin A6 and has a 10k resistor in series with it. 
- The onboard USB single cell lithium ion battery charger is set to charge at about 800mAh

![hmm](Images/identifiers.png)


## **Things to know about the supplied breakout board**
- The green PCF8563 & NRF24l01 breakout board should fit right onto the male headers on the black microcontroller boards
- On breakout boards with an NRF24l01 Transceiver there are two jumpers next to it to set the CSN and CE pins. They come set as D7 & D6
- On the back of the board there is also a small jumper that can be bridged with solder to enable the RTC's interrupt pin on D2
- The battery size is CR1220, they can be found pretty cheap with pre welded tabs for easy soldering
- the buzzer or speaker is wired to pin A3 and can be used to make noise using the tone() function
- There is an I2C PCF8563 Real time clock that can be used to keep time, wake the microcontroller using the interrupt pin at set intervals, etc.


##### I am working on putting together pinouts and more concise documentation for each board to make getting started a bit easier, but in the meantime if you have any questions or suggestins please don't be shy! 

