/*
 * This example is one of the sketches used during the testing of each board prior to shipping. 
 * This sketch allows you to quickly verify that the PCF8574 and connnected buttons and LED are working,
 * along with the TFT display and micro SD card slot/inserted card. Of course if these are all functioning
 * than we can deduce that the SPI and I2C functionality of the MCU are also properly working allowing us 
 * to quickly test several key aspects of the board!
 * 
 * You can utilize parts of this sketch to see how to poll the inputs of the PCF8574 i2c IO expander,
 * and toggle the LED connected to it when a button is pressed, read SD card contents, and prepare/use
 * the display for text.
 */

#include <SPI.h>
#include <Wire.h>
#include <SD.h>
#include <TFT.h>
#include "PCF8574.h"

File root;
int direction = 0;
int lastNum;
int pinRead;

TFT tft = TFT(TFT_CS, TFT_DC, TFT_RST);
PCF8574 pcf(0x20);

void setup() {
 // SD.begin(SD_CS);
  tft.begin();
  pcf.begin();
  tft.fillScreen(BG);
  tft.setRotation(3);
  tft.stroke(POWDER);
  tft.text("SD & Function Test", 10, 1);
  tft.line(0, 17, 160, 17);
  tft.textSize(2);
  tft.setTextColor(TXT, BG);
  pinMode(SD_CS, INPUT);
  pinMode(TFT_BL, OUTPUT);
  analogWrite(TFT_BL, 182);
  if (SD.begin(SD_CS) == true) {
    root = SD.open("/");
    listFile(root);
  } else {
    tft.setCursor(45, 100);
    tft.print("No SD");
  }
}

void loop() {
  for(int i = 0; i < 7; i++) {
    if(pcf.readButton(i) != 1) {
      direction = i;
      pcf.write(i, HIGH);
    }
  }
  
  if (direction != lastNum) {
    tft.setCursor(20, 40);
    pcf.toggle(7);
    switch (direction) {
      case 0:
        tft.print("RSet     ");
        break;
      case 1:
        tft.print("Right  ");
        break;
      case 2:
        tft.print("Down  ");
        break;
      case 3:
        tft.print("LSet   ");
        break;
      case 4:
        tft.print("Up     ");
        break;
      case 5:
        tft.print("Left   ");
        break;
      case 7:
        tft.print("Center");
        break;
    }
    tone(A3, 852, 150);
    delay(100);
    tone(A3, 1457, 100);
    //delay(1000);
    lastNum = direction;
  }
  pinRead = analogRead(A0);
  tft.setCursor(80, 80);
  tft.print(pinRead);
}

void listFile(File dir) {
  while (true) {
    File entry = dir.openNextFile();
    if (!entry) {
      break;
    }
    tft.setCursor(0, 100);
    tft.print(entry.name());
    entry.close();
  }
}
