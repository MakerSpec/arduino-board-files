//replace "filename.bmp" with the exact name for your desired bitmap image
//located in the root directory(not inside any folders) of your SD card

#include <SD.h>
#include <TFT.h>
#include <Wire.h>
#include <SPI.h>
#include "PCF8574.h"
#include <Adafruit_BMP085.h>
//#include <EEPROM.h>
#include <avr/sleep.h>

PCF8574 pcf(0x20);
TFT tft = TFT(10, 9, 8);//pin 12 for v1 328PTFT, pin 8 for v2 and up
Adafruit_BMP085 bmp;

bool sdFlag = 0;
bool wait = 0;
byte lastButton = 9;
byte mode = 0;
byte y = 0;
byte c = 200;
int button = 9;
File root;
unsigned long idleTimer = 0;

PImage img;

const unsigned char PROGMEM storm [] = {
  //48x48px
  0x00, 0x00, 0x03, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff,
  0x00, 0x00, 0x00, 0x00, 0xff, 0x3f, 0xc0, 0x00, 0x00, 0x01, 0xf8, 0x07, 0xe0, 0x00, 0x00, 0x03,
  0xe0, 0x01, 0xf0, 0x00, 0x00, 0x07, 0xc0, 0x00, 0xf8, 0x00, 0x00, 0xff, 0x00, 0x00, 0x3f, 0x80,
  0x03, 0xfe, 0x00, 0x00, 0x1f, 0xe0, 0x0f, 0xfc, 0x00, 0x00, 0x0f, 0xf8, 0x1f, 0x00, 0x00, 0x00,
  0x00, 0x7c, 0x3c, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x78, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x70, 0x00,
  0x00, 0x00, 0x00, 0x0e, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07,
  0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00,
  0x00, 0x00, 0x00, 0x07, 0x70, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x78, 0x00, 0x00, 0x00, 0x00, 0x0e,
  0x3c, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x0f, 0x03, 0xc0, 0x00, 0x78, 0x7c, 0x0f, 0xff, 0xe0, 0x08,
  0xff, 0xf8, 0x07, 0xff, 0xf0, 0x19, 0xff, 0xe0, 0x01, 0xf8, 0xf8, 0x33, 0xc7, 0x80, 0x00, 0x00,
  0x30, 0x73, 0x80, 0x00, 0x00, 0x00, 0x00, 0xe3, 0x00, 0x00, 0x00, 0x00, 0x01, 0xe0, 0x00, 0x00,
  0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x80,
  0x00, 0x00, 0x00, 0x00, 0x1f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xc0, 0x00, 0x00, 0x00, 0x00,
  0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00,
  0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00
};

//this function will be called anytime the interrupt is triggered
//by doing this it will wake the processor and reset the sleep timer
//so it remains awake for 15 seconds after any button press!
ISR(PCINT2_vect) {
  //PinChangeInterrupt to wake from sleep
  idleTimer = millis();
}

void setup() {
  tft.begin();
  tft.initR(INITR_BLACKTAB);
  bmp.begin();
  pcf.begin();
  pinMode(5, OUTPUT);
  pinMode(4, INPUT_PULLUP);
  //setup pin change interrupt on pin D4 which is connected to the
  //PCF8574 INT pin via a 0ohm jumper on the front of the board
  *digitalPinToPCMSK(4) |= bit (digitalPinToPCMSKbit(4));
  PCIFR |= bit (digitalPinToPCICRbit(4));
  PCICR |= bit (digitalPinToPCICRbit(4));
  dispInit();
  analogWrite(5, 180); //turn on display backlight internally connected to D5
  if (SD.begin(A0) != true) {
    sdFlag = 1;
  } else {
    sdFlag = 0;
    //change filename.bmp to the name of the properly formatted image
    //you have saved to the root of your microSD card
    img = tft.loadImage("filename.bmp");
  }
}


void setSleep() {
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  //display BL off
  analogWrite(5, 0);
  //naptime
  sleep_cpu(); //anything beyond this point happens after an interrupt is triggered and the processor wakes
  //turn on backlight again
  analogWrite(5, 180);
  //resume normal
}

void dispText(const char * text, byte tX, byte tY, byte textSize, int color) {
  tft.stroke(color);
  tft.setTextSize(textSize);
  tft.text(text, tX, tY);
}

/*
  void dispPrint(const String text, byte x, byte y, byte s, int t, int bg) {
  tft.setTextColor(t, bg);
  tft.setTextSize(s);
  tft.print(text, x, y);
  }
*/

void loop() {
  for (int a = 0; a < 7; a++) {
    if (pcf.readButton(a) != 1) {
      button = a;
      idleTimer = millis();
    }
  }
  if ((button == 0) || (button == 4)) {
    dispInit();
    wait = 0;
    if ((button == 0) && (mode < 9)) {
      mode += 1;
    } else if ((button == 4) && (mode > 0)) {
      mode -= 1;
    }
  }

  if ((mode == 8) && (wait == 1)) {
    if ((button == 3) && (c >= 38)) {
      c -= 54;
      wait = 0;
    } else if ((button == 5) && (c <= 200)); {
      c += 54;
      wait = 0;
    }
  }

  if (wait == 0) { //when wait flag is set display doesn't update until user input
    if (mode == 0) { //intro & sleep/wake
      dispText("328PBnJ Demo", 7, 1, 2, NUKE);
      dispText("ACTIVE", 50, 39, 2, BLOOD);
      dispText("15 second sleep timer", 25, 30, 1, BLOOD);
      dispText("Press any button to wake", 16, 55, 1, BLOOD);
    } else if (mode == 1) { //pcf8574
      dispText("PCF8574 Demo", 12, 0, 2, AZUL);
      if ((button < 7) && (button != lastButton)) {
        pcf.toggle(7);
        dispText("Pin#: ", 14, 30, 2, AZUL);
        tft.setTextColor(NUKE, BLACK);
        tft.setCursor(80, 30);
        tft.print(button);
        tft.setCursor(17, 70);
        //add switch case for direction
        switch (button) {
          case 0:
            tft.print("RSet  ");
            break;
          case 1:
            tft.print("Right ");
            break;
          case 2:
            tft.print("Left  ");
            break;
          case 3:
            tft.print("Down  ");
            break;
          case 4:
            tft.print("LSet  ");
            break;
          case 5:
            tft.print("UP    ");
            break;
          case 6:
            tft.print("Center");
            break;
        }
        lastButton = button;
      }
    } else if (mode == 2) {
      dispText("BMP Temperature:", 34, 20, 1, NUKE);
      dispText("Barometric Pressure:", 26, 50, 1, NUKE);
      dispText("BMP Demo", 37, 1, 2, NANA);
      tft.setTextColor(EPURP, BLACK);
      tft.setCursor(5, 30);
      tft.print(bmp.readTemperature());
      tft.print("C");
      tft.setCursor(88, 30);
      tft.print((bmp.readTemperature() * 1.8) + 32);
      tft.print("F");
      tft.setCursor(38, 61);
      tft.print(bmp.readPressure() / 3386.389F);
    } else if (mode == 3) {
      //BMP graphics from flash
      dispText("Flash Bitmap", 20, 1, 2, DGRAY);
      //      tft.text("Flash Bitmap", 20, 1);
      tft.drawBitmap(56, 40, storm, 48, 48, CARBON);
    } else if (mode == 4) {
      //BMP graphics from SD
      dispText("SD Bitmap", 35, 1, 2, DEEPSEA);
      //      tft.text("SD Bitmap", 35, 1);
      //   tft.setTextSize(1);
      if (sdFlag == 1) {
        dispText("NO", 65, 28, 3, BLOOD);
        dispText("SD", 65, 83, 3, BLOOD);
      } else if (sdFlag == 0) {
        if (img.isValid()) {
        }
        //display the image
        tft.image(img, 0, 12);
        wait = 1; //wait to prevent constant redrawing of the image
      } else {
        dispText("Image", 38, 34, 3, BLOOD);
        dispText("error", 38, 80, 3, BLOOD);
        dispText("Ensure the file name &", 25, 84, 1, BLOOD);
        dispText("format are correct", 35, 93, 1, BLOOD);
      }
    } else if (mode == 5) {
      //SD file list
      dispText("SD File List", 2, 1, 2, DGRAY);
      //      tft.text("SD File List", 2, 1);
      if (sdFlag == 1) {
        dispText("NO", 65, 28, 3, BLOOD);
        dispText("SD", 65, 83, 3, BLOOD);
      } else {
        root = SD.open("/");
        listFiles(root);
      }
      wait = 1; //wait to prevent constantly rereading the SD card
    } else if (mode == 6) {
      //LDR test mode
      dispText("LDR Demo", 40, 1, 2, LIME);
      dispText("LDR Value: ", 22, 30, 2, SEAFOAM);
      tft.setTextSize(3);
      tft.setCursor(50, 50);
      tft.setTextColor(DBROWN, BLACK);
      tft.print(analogRead(A6));
    } else if (mode == 7) {
      dispText("Display Brightness", 2, 1, 2, DEEPSEA);

    } else if (mode == 8) {
      //ASCII characters demo
      dispText("ASCII Table", 4, 1, 2, BURNT);
      y = 18;
      byte x = 0;
      byte i = c + 54;
      tft.setTextSize(1);
      tft.setCursor(0, 0);
      tft.print(button);
      tft.setCursor(x, y);
      tft.setTextColor(WHITE, BLACK);
      while (i > c) {
        i -= 1;
        tft.print(i);
        x += 18;
        tft.print(char(i));
        x += 8;
        if (x > 140) {
          x = 0;
          y += 10;
        }
        tft.setCursor(x, y);
      }
      wait = 1;
      tft.setCursor(150, 0);
      tft.print(wait);
    } else if (mode == 9) {
      //eeprom demo
    } else if (mode == 10) {
      //nrf24 demo
    }
  }

  //15 second idle sleep timer which ensures the PCF interrupt is working if
  //the microcontroller can be brought out of sleep mode with a button press
  if (millis() - idleTimer > 15000) {
    setSleep();
  }
  button = 9;
}
