void listFiles(File dir) {
  y = 20;
  while (true) {
    File entry = dir.openNextFile();
    if (!entry) {
      break;
    }
    tft.setTextColor(AZUL, BLACK);
    tft.setTextSize(1);
    tft.setCursor(0, y);
    tft.print(entry.name());
    entry.close();
    y += 8;
    if (y >= 112) y = 18;
  }
}
