void dispInit() {
  //clear and initalize display with static text
  tft.fillScreen(BLACK);
  tft.stroke(DGRAY);
  tft.line(0, 16, 160, 16); //horizontal line under title
  tft.line(0, 118, 25, 118); //horizontal line over "back"
  tft.line(25, 118, 25, 128); //vertical line next to "back"
  tft.line(114, 118, 160, 118); //horizontal line next to "forward"
  tft.line(114, 118, 114, 128); //vertical lien next to "forward"
  dispText("Back", 1, 120, 1, WHITE);
  dispText("Forward", 117, 120, 1, WHITE);
  tft.setTextSize(2);
}
