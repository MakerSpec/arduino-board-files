
// =============================================================================
//
// Arduino - Crappy Bird
// ---------------------------
// This is a modified version of Themistokle "mrt-prodz" Benetatos Flappy Bird Clone
// made to work on my 328PTFT & 328PBnJ boards.This will work with the libraries 
// packaged with their Arduino board files and shouldn't require any additional 
// libraries or tweaks. 
// --------------------
//
// http://mrt-prodz.com
//
// =============================================================================

#include <TFT.h> 
#include <SPI.h>
#include <Wire.h>
#include "PCF8574.h"
#include <EEPROM.h>

#define up 5
#define down 3
#define left 2
#define right 1
#define center 6
#define rSet 0
#define lSet 4

/*
  //Model E - low profile nav switch
  #define up 5
  #define down 1
  #define left 6
  #define right 2
  #define center 3
  #define rSet 0
  #define lSet 4
 */


//pcf8574 on i2x address 0x20
PCF8574 pcf(0x20);

#define TFT_CS           10
#define TFT_DC            9     // Sainsmart RS/DC
#define TFT_RST           8     // Sainsmart RES

// initialize screen with pins
TFT tft = TFT(TFT_CS,  TFT_DC, TFT_RST);
// instead of using TFT.width() and TFT.height() set constant values
// (we can change the size of the game easily that way)
#define TFTW            128   // screen width
#define TFTH            160    // screen height
#define TFTW2            64     // half screen width
#define TFTH2            80     // half screen height
// game constant
#define SPEED             1
#define GRAVITY         9.8
#define JUMP_FORCE     2.15
#define SKIP_TICKS     30.0     // 1000 / 50fps orig 20.0
#define MAX_FRAMESKIP     1
// bird size
#define BIRDW             8     // bird width
#define BIRDH             8     // bird height
#define BIRDW2            4     // half width
#define BIRDH2            4     // half height
// pipe size
#define PIPEW            12     // pipe width
#define GAPHEIGHT        36     // pipe gap height
// floor size
#define FLOORH           20     // floor height (from bottom of the screen)
// grass size
#define GRASSH            4     // grass height (inside floor, starts at floor y)

// background
const unsigned int BCKGRDCOL = NIGHTSKY;
// bird
const unsigned int BIRDCOL = YELLOW;
// pipe
const unsigned int PIPECOL  = LIME;
// pipe highlight
const unsigned int PIPEHIGHCOL  = WHITE;
// pipe seam
const unsigned int PIPESEAMCOL  = DGRAY;
// floor
const unsigned int FLOORCOL = BURNT;
// grass (col2 is the stripe color)
const unsigned int GRASSCOL  = GREEN;
const unsigned int GRASSCOL2 = LIME;
int highscore = 0;

// bird sprite
// bird sprite colors (Cx name for values to keep the array readable)
#define C0 AZUL
#define C1 DRAB
#define C2 YELLOW
#define C3 WHITE
#define C4 RED
#define C5 BLOOD

static unsigned int birdcol[] =
{ C0, C0, C1, C1, C1, C1, C1, C0,
  C0, C1, C2, C2, C2, C1, C3, C1,
  C0, C2, C2, C2, C2, C1, C3, C1,
  C1, C1, C1, C2, C2, C3, C1, C1,
  C1, C2, C2, C2, C2, C2, C4, C4,
  C1, C2, C2, C2, C1, C5, C4, C0,
  C0, C1, C2, C1, C5, C5, C5, C0,
  C0, C0, C1, C5, C5, C5, C0, C0};

// bird structure
static struct BIRD {
  unsigned char x, y, old_y;
  unsigned int col;
  float vel_y;
} bird;
// pipe structure
static struct PIPE {
  char x, gap_y;
  unsigned int col;
} pipe;

// score
static short score;
// temporary x and y var
static short tmpx, tmpy;

// ---------------
// draw pixel
// ---------------
// faster drawPixel method by inlining calls and using setAddrWindow and pushColor
// using macro to force inlining
//#define drawPixel(a, b, c) TFT.setAddrWindow(a, b, a, b); TFT.pushColor(c)

// ---------------
// initial setup
// ---------------
void setup() {
  // initialize a ST7735 display
  tft.initR(INITR_BLACKTAB);
  tft.setRotation(4);
  //set backlight brightness
  analogWrite(5, 220);
  //initialize PCF8574
  pcf.begin();
  EEPROM.get(3, highscore);
  if ((isnan(highscore)) || (highscore < 0)) {
    highscore = 0;
    EEPROM.put(3, highscore);
  }
}

// ---------------
// main loop
// ---------------
void loop() {
  game_start();
  game_loop();
  game_over();
}

// ---------------
// game loop
// ---------------
void game_loop() {
  // ===============
  // prepare game variables
  // draw floor
  // ===============
  // instead of calculating the distance of the floor from the screen height each time store it in a variable
  unsigned char GAMEH = TFTH - FLOORH;
  // draw the floor once, we will not overwrite on this area in-game
  // black line
  tft.drawFastHLine(0, GAMEH, TFTW, BCKGRDCOL );
  // grass and stripe
  tft.fillRect(0, GAMEH+1, TFTW2, GRASSH, GRASSCOL);
  tft.fillRect(TFTW2, GAMEH+1, TFTW2, GRASSH, GRASSCOL2);
  // black line
  tft.drawFastHLine(0, GAMEH+GRASSH, TFTW, BCKGRDCOL);
  // mud
  tft.fillRect(0, GAMEH+GRASSH+1, TFTW, FLOORH-GRASSH, FLOORCOL);
  // grass x position (for stripe animation)
  char grassx = TFTW;
  // game loop time variables
  double delta, old_time, next_game_tick, current_time;
  next_game_tick = current_time = millis();
  int loops;
  // passed pipe flag to count score
  bool passed_pipe = false;
  // temp var for setAddrWindow
  unsigned char px;
  
  while (1) {
    loops = 0;
    while( millis() > next_game_tick && loops < MAX_FRAMESKIP) {
      // ===============
      // input
      // ===============
   if (pcf.readButton(center) == 0) {
        // if the bird is not too close to the top of the screen apply jump force
        if (bird.y > BIRDH2*0.5) bird.vel_y = -JUMP_FORCE;
        // else zero velocity
        else bird.vel_y = 0;
      }
      // ===============
      // update
      // ===============
      // calculate delta time
      // ---------------
      old_time = current_time;
      current_time = millis();
      delta = (current_time-old_time)/1000;

      // bird
      // ---------------
      bird.vel_y += GRAVITY * delta;
      bird.y += bird.vel_y;

      // pipe
      // ---------------
      pipe.x -= SPEED;
      // if pipe reached edge of the screen reset its position and gap
      if (pipe.x < -PIPEW) {
        pipe.x = TFTW;
        pipe.gap_y = random(10, GAMEH-(10+GAPHEIGHT));
      }

      // ---------------
      next_game_tick += SKIP_TICKS;
      loops++;
    }

    // ===============
    // draw
    // ===============
    // pipe
    // ---------------
    // we save cycles if we avoid drawing the pipe when outside the screen
    if (pipe.x >= 0 && pipe.x < TFTW) {
      // pipe color
      tft.drawFastVLine(pipe.x+3, 0, pipe.gap_y, PIPECOL);
      tft.drawFastVLine(pipe.x+3, pipe.gap_y+GAPHEIGHT+1, GAMEH-(pipe.gap_y+GAPHEIGHT+1), PIPECOL);
      // highlight
      tft.drawFastVLine(pipe.x, 0, pipe.gap_y, PIPEHIGHCOL);
      tft.drawFastVLine(pipe.x, pipe.gap_y+GAPHEIGHT+1, GAMEH-(pipe.gap_y+GAPHEIGHT+1), PIPEHIGHCOL);
      // bottom and top border of pipe
      tft.drawPixel(pipe.x, pipe.gap_y, PIPESEAMCOL);
      tft.drawPixel(pipe.x, pipe.gap_y+GAPHEIGHT, PIPESEAMCOL);
      // pipe seam
      tft.drawPixel(pipe.x, pipe.gap_y-6, PIPESEAMCOL);
      tft.drawPixel(pipe.x, pipe.gap_y+GAPHEIGHT+6, PIPESEAMCOL);
      tft.drawPixel(pipe.x+3, pipe.gap_y-6, PIPESEAMCOL);
      tft.drawPixel(pipe.x+3, pipe.gap_y+GAPHEIGHT+6, PIPESEAMCOL);
    }
    // erase behind pipe
    if (pipe.x <= TFTW) tft.drawFastVLine(pipe.x+PIPEW, 0, GAMEH, BCKGRDCOL);

    // bird
    // ---------------
    tmpx = BIRDW-1;
    do {
          px = bird.x+tmpx+BIRDW;
          // clear bird at previous position stored in old_y
          // we can't just erase the pixels before and after current position
          // because of the non-linear bird movement (it would leave 'dirty' pixels)
          tmpy = BIRDH - 1;
          do {
            tft.drawPixel(px, bird.old_y + tmpy, BCKGRDCOL);
          } while (tmpy--);
          // draw bird sprite at new position
          tmpy = BIRDH - 1;
          do {
            tft.drawPixel(px, bird.y + tmpy, birdcol[tmpx + (tmpy * BIRDW)]);
          } while (tmpy--);
    } while (tmpx--);
    // save position to erase bird on next draw
    bird.old_y = bird.y;

    // grass stripes
    // ---------------
    grassx -= SPEED;
    if (grassx < 0) grassx = TFTW;
    tft.drawFastVLine( grassx    %TFTW, GAMEH+1, GRASSH-1, GRASSCOL);
    tft.drawFastVLine((grassx+64)%TFTW, GAMEH+1, GRASSH-1, GRASSCOL2);

    // ===============
    // collision
    // ===============
    // if the bird hit the ground game over
    if (bird.y > GAMEH-BIRDH) break;
    // checking for bird collision with pipe
    if (bird.x+BIRDW >= pipe.x-BIRDW2 && bird.x <= pipe.x+PIPEW-BIRDW) {
      // bird entered a pipe, check for collision
      if (bird.y < pipe.gap_y || bird.y+BIRDH > pipe.gap_y+GAPHEIGHT) break;
      else passed_pipe = true;
    }
    // if bird has passed the pipe increase score
    else if (bird.x > pipe.x+PIPEW-BIRDW && passed_pipe) {
      passed_pipe = false;
      // erase score with background color
      tft.setTextColor(BCKGRDCOL);
      tft.setCursor(TFTW2, 4);
      tft.print(score);
      // set text color back to white for new score
      tft.setTextColor(WHITE);
      // increase score since we successfully passed a pipe
      score++;
      
    }

    // update score
    // ---------------
    tft.setCursor(TFTW2, 4);
    tft.print(score);
  }
  
  if(score > highscore){
    //store new highscore
    highscore = score;
    EEPROM.put(3, highscore);
    //flash LED
    pcf.toggle(7);
    delay(150);
    pcf.toggle(7);
    delay(150);
    pcf.toggle(7);
    delay(150);
    pcf.toggle(7);
  } else {
  //small delay to show how the player lost
  delay(1200);
  }
}

// ---------------
// game start
// ---------------
void game_start() {
  tft.fillScreen(BCKGRDCOL);
  tft.fillRect(10, TFTH2 - 20, TFTW-20, 1, WHITE);
  tft.fillRect(10, TFTH2 + 32, TFTW-20, 1, WHITE);
  tft.setTextColor(WHITE);
  tft.setTextSize(3);
  // half width - num char * char width in pixels
  tft.setCursor( TFTW2-(6*9), TFTH2 - 16);
  tft.println("CRAPPY");
  tft.setTextSize(3);
  tft.setCursor( TFTW2-(6*9), TFTH2 + 8);
  tft.println("-BIRD-");
  tft.setTextSize(0);
  tft.setCursor( 12, TFTH2 - 28);
  tft.println("MakerSpec");
  tft.setCursor( 28, TFTH2 + 34);
  tft.println("press center");
  while (pcf.readButton(center) != 0) {
    // wait for push button to start game
    //press right tact switch to clear highscore memory
    if(pcf.readButton(rSet) == 0){
      highscore = 0;
      EEPROM.put(3, highscore);
    }
    if(pcf.readButton(center) == 0) {
      break;
    }
  }
  
  // init game settings
  game_init();
}

// ---------------
// game init
// ---------------
void game_init() {
  // clear screen
  tft.fillScreen(BCKGRDCOL);
  // reset score
  score = 0;
  // init bird
  bird.x = 20;
  bird.y = bird.old_y = TFTH2 - BIRDH;
  bird.vel_y = -JUMP_FORCE;
  tmpx = tmpy = 0;
  // generate new random seed for the pipe gap
  randomSeed(analogRead(0));
  // init pipe
  pipe.x = TFTW;
  pipe.gap_y = random(20, TFTH-60);
}

// ---------------
// game over
// ---------------
void game_over() {
  tft.fillScreen(BCKGRDCOL);
  tft.setTextColor(WHITE);
  tft.setTextSize(2);
  // half width - num char * char width in pixels
  tft.setCursor( TFTW2 - (9*6), TFTH2 - 4);
  tft.println("GAME OVER");
  tft.setTextSize(0);
  tft.setCursor(TFTW2 - 27, 12);
  tft.print("score: ");
  tft.print(score);
  tft.setCursor(TFTW2 - 40, 4);
  tft.print("highscore: ");
  tft.print(highscore);
  tft.setCursor( TFTW2 - (12*3), TFTH2 + 12);
  tft.println("press button");
  while (pcf.readButton(center) != 0) {
    // wait for push button
   if(pcf.readButton(center) == 0) {
   break; }
  }
}
