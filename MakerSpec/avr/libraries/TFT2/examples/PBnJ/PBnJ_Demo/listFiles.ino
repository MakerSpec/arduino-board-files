void listFiles(File dir) {
  while (true) {
    File entry = dir.openNextFile();
    if (!entry) {
      break;
    }
    tft.setCursor(0, 100);
    tft.print(entry.name());
    entry.close();
  }
}
