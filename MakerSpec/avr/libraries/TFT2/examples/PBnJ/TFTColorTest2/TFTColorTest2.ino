#include <TFT.h>
#include <SPI.h>

TFT tft = TFT(10, 9, 8);
int y = 0;
int x = 0;

void setup() {
  pinMode(5, OUTPUT);
  analogWrite(5, 150);
  tft.begin();
  tft.setRotation(3);
  // put your setup code here, to run once:

}

void loop() {
  for(int clr = 0; clr < 65000; clr++) {
    tft.drawPixel(x, y, clr);
    y++;
    if(y > 128) {
      y = 0;
      x++;
    }
    if(x > 168) x = 0;
  }
}
