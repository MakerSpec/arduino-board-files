#include <TFT.h>
#include <SPI.h>

TFT tft = TFT(10, 9, 8); //setup tft with internally wired CS, DC and RST pins

void setup() {
  tft.initR(INITR_MINI160x80); //initialize .96" TFT in landscape mode
  tft.setRotation(1);
  tft.setTextSize(2);
  pinMode(5, OUTPUT); //set internally connected backlight pin to output
  tft.fillScreen(CARBON);
  tft.stroke(EPURP);
  tft.text("Hello World", 11, 0); //print text starting with top leftmost pixel in column 11, row 0
  analogWrite(5, 220); //turn on display backlight
}

void loop() {
  tft.stroke(DBROWN);
  tft.text("Up Time: ", 11, 25);
  tft.setCursor(101 , 25);
  tft.setTextColor(DBROWN, CARBON);
  tft.print(millis() / 1000);
}
