#include <TFT.h>
#include <SPI.h>

TFT tft = TFT(10, 9, 8);

double degreesF = 0.00;
unsigned int timer = 0;
byte count = 0;


void thermistor() {
  double steinhart = analogRead(A6);

  steinhart = 1023 / steinhart - 1;
  steinhart = 995 / steinhart; //series resistor / reading
  steinhart /= 10000;      // / nominal ntc res.
  steinhart = log(steinhart);     // ln(R/Ro)
  steinhart /= 3435;               // / Bcoeffient
  steinhart += 1.0 / (25 + 273.15);// + (1/To)
  steinhart = 1.0 / steinhart;    // Invert
  steinhart -= 273.15;             // convert to C
  degreesF += (steinhart * 1.8F + 32); //convert C to F
}


void setup() {
  pinMode(5, OUTPUT);
  Serial.begin(9600);
  Serial.println("Serial Coms started");
  tft.initR(INITR_MINI160x80);
  tft.setRotation(1);
  tft.setTextSize(1);
  pinMode(A4, INPUT);
  tft.fillScreen(CARBON);
  tft.stroke(BLOOD);
  tft.text("On Board Thermistor Demo", 11, 1);
  tft.setTextSize(2);
  tft.setTextColor(AZUL, CARBON);
  analogWrite(5, 220);
}

void loop() {
  //grab temp 10 times a second
  tft.setCursor(10, 55);
  tft.print(degreesF);
  if (timer - millis() >= 100) {
    thermistor();
    count += 1;
    timer = millis();
  }

  //average out all 10 readings and update display
  if (count == 10) {
    degreesF /= 10;
    tft.setCursor(4, 30);
    tft.print("NTC Avg:");
    tft.print(degreesF);
    Serial.print("NTC Temp: ");
    Serial.println(degreesF);
    count = 0;
    degreesF = 0;
  }
}
