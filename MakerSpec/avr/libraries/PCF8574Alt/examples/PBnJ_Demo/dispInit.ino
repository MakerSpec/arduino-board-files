void dispInit() {
  //clear and initalize display with static text
  tft.fillScreen(BLACK);
  tft.stroke(DGRAY);
  tft.line(0, 16, 160, 16); //horizontal line under title
  tft.line(0, 118, 23, 118); //horizontal line over "back"
  tft.line(23, 118, 23, 128); //vertical line next to "back"
  tft.line(119, 118, 160, 118); //horizontal line next to "forward"
  tft.line(119, 118, 119, 128); //vertical lien next to "forward"
  tft.setTextSize(1);
  tft.text("Back", 1, 120);
  tft.text("Forward", 122, 120);
  tft.setTextSize(2);
}
