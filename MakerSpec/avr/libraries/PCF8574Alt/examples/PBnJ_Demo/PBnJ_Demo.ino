//replace "filename.bmp" with the exact name for your desired bitmap image
//located in the root directory(not inside any folders) of your SD card


#include <SD.h>
#include <TFT.h>
#include <Wire.h>
#include <SPI.h>
#include "PCF8574.h"
#include <Adafruit_BMP085.h>
//#include <EEPROM.h>
#include <avr/sleep.h>

PCF8574 pcf(0x20);
TFT tft = TFT(10, 9, 8);//pin 12 for v1 328PTFT, pin 8 for v2 and up
Adafruit_BMP085 bmp;

bool sdFlag = 0;
bool wait = 0;
byte lastButton = 9;
byte mode = 0;
File root;
unsigned long idleTimer = 0;


PImage img;

const unsigned char PROGMEM storm [] = {
  //48x48px
  0x00, 0x00, 0x03, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x3f, 0xff,
  0x00, 0x00, 0x00, 0x00, 0xff, 0x3f, 0xc0, 0x00, 0x00, 0x01, 0xf8, 0x07, 0xe0, 0x00, 0x00, 0x03,
  0xe0, 0x01, 0xf0, 0x00, 0x00, 0x07, 0xc0, 0x00, 0xf8, 0x00, 0x00, 0xff, 0x00, 0x00, 0x3f, 0x80,
  0x03, 0xfe, 0x00, 0x00, 0x1f, 0xe0, 0x0f, 0xfc, 0x00, 0x00, 0x0f, 0xf8, 0x1f, 0x00, 0x00, 0x00,
  0x00, 0x7c, 0x3c, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x78, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x70, 0x00,
  0x00, 0x00, 0x00, 0x0e, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07,
  0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xe0, 0x00,
  0x00, 0x00, 0x00, 0x07, 0x70, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x78, 0x00, 0x00, 0x00, 0x00, 0x0e,
  0x3c, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x0f, 0x03, 0xc0, 0x00, 0x78, 0x7c, 0x0f, 0xff, 0xe0, 0x08,
  0xff, 0xf8, 0x07, 0xff, 0xf0, 0x19, 0xff, 0xe0, 0x01, 0xf8, 0xf8, 0x33, 0xc7, 0x80, 0x00, 0x00,
  0x30, 0x73, 0x80, 0x00, 0x00, 0x00, 0x00, 0xe3, 0x00, 0x00, 0x00, 0x00, 0x01, 0xe0, 0x00, 0x00,
  0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x80,
  0x00, 0x00, 0x00, 0x00, 0x1f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xc0, 0x00, 0x00, 0x00, 0x00,
  0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x00,
  0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00
};

//this function will be called anytime the interrupt is triggered
//by doing this it will wake the processor and reset the sleep timer
//so it remains awake for 15 seconds after any button press!
ISR(PCINT2_vect) {
  //PinChangeInterrupt to wake from sleep
  idleTimer = millis();
}

void setup() {
  tft.begin();
  tft.initR(INITR_BLACKTAB);
  bmp.begin();
  pcf.begin();
  pinMode(5, OUTPUT);
  pinMode(4, INPUT_PULLUP);
  //setup pin change interrupt on pin D4 which is connected to the
  //PCF8574 INT pin via a 0ohm jumper on the front of the board
  *digitalPinToPCMSK(4) |= bit (digitalPinToPCMSKbit(4));
  PCIFR |= bit (digitalPinToPCICRbit(4));
  PCICR |= bit (digitalPinToPCICRbit(4));
  dispInit();
  analogWrite(5, 180); //turn on display backlight internally connected to D5
  if (SD.begin(A0) != true) {
    sdFlag = 1;
  } else {
    sdFlag = 0;
    img = tft.loadImage("sdImage");
  }
}


void setSleep() {
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  //display BL off
  analogWrite(5, 0);
  //naptime
  sleep_cpu(); //anything beyond this point happens after an interrupt is triggered and the processor wakes
  //turn on backlight again
  analogWrite(5, 180);
  //resume normal
}

void loop() {
  int button = 9;
  for (int i = 0; i < 7; i++) {
    if (pcf.readButton(i) != 1) {
      button = i;
      idleTimer = millis();
    }
  }
  if ((button == 0) || (button == 4)) {
    dispInit();
    wait = 0;
    if ((button == 0) && (mode < 6)) {
      mode += 1;
    } else if ((button == 4) || (mode > 0)) {
      mode -= 1;
    }
  }

  if (wait != 1) {
    if (mode == 0) { //intro & sleep/wake
      tft.stroke(NUKE);
      tft.setTextSize(2);
      tft.text("328PBnJ Demo", 7, 1);
      tft.stroke(BLOOD);
      tft.text("ACTIVE", 50, 38);
      tft.setTextSize(1);
      tft.text("15 second sleep timer", 25, 30);
      tft.text("Press any button to wake", 20, 55);
    } else if (mode == 1) { //pcf8574
      tft.stroke(AZUL);
      tft.text("PCF8574 Demo", 12, 0);
      if ((button < 7) && (button != lastButton)) {
        pcf.toggle(7);
        tft.text("Pin#: ", 14, 30);
        tft.setTextColor(NUKE, BLACK);
        tft.print(button);
        tft.setCursor(17, 70);
        //add switch case for direction
        switch (button) {
          case 0:
            tft.print("RSet  ");
            break;
          case 1:
            tft.print("Right ");
            break;
          case 2:
            tft.print("Left  ");
            break;
          case 3:
            tft.print("Down  ");
            break;
          case 4:
            tft.print("LSet  ");
            break;
          case 5:
            tft.print("UP    ");
            break;
          case 6:
            tft.print("Center");
            break;
        }
      }
      lastButton = button;
    } else if (mode == 2) {
      tft.stroke(NANA);
      tft.text("BMP Demo", 12, 1);
      tft.setTextSize(1);
      tft.stroke(NUKE);
      tft.text("BMP Temperature:", 40, 20);
      tft.text("Barometric Pressure:", 30, 50);
      tft.setTextSize(2);
      tft.setTextColor(EPURP, BLACK);
      tft.setCursor(5, 30);
      tft.print(bmp.readTemperature());
      tft.print("C");
      tft.setCursor(65, 30);
      tft.print((bmp.readTemperature() * 1.8) + 32);
      tft.print("F");
      tft.setCursor(38, 52);
      tft.print(bmp.readPressure() / 3386.389F);
    } else if (mode == 3) {
      //BMP graphics from flash
      //    tft.setTextSize(1);
      tft.text("Flash Bitmap", 20, 1);
      tft.drawBitmap(56, 40, storm, 48, 48, CARBON);
    } else if (mode == 4) {
      //BMP graphics from SD
      tft.text("SD Bitmap", 35, 1);
      //   tft.setTextSize(1);
      if (sdFlag == 1) {
        tft.setTextSize(3);
        tft.stroke(BLOOD);
        tft.text("NO", 65, 28);
        tft.text("SD", 65, 104);
      } else if (sdFlag == 0) {
        if (img.isValid()) {
        }
        //display the image
        tft.image(img, 20, 12);
        wait = 1; //wait to prevent constant redrawing of the image
      } else {
        tft.setTextSize(3);
        tft.stroke(BLOOD);
        tft.text("Image", 38, 34);
        tft.text("error", 38, 80);
        tft.setTextSize(1);
        tft.text("Ensure the file name &", 25, 84);
        tft.text("format are correct", 35, 93);
        tft.setTextSize(1);
      }
    } else if (mode == 5) {
      //SD file list
      tft.text("SD File List", 2, 1);
      if (sdFlag == 1) {
        tft.setTextSize(3);
        tft.stroke(BLOOD);
        tft.text("NO", 65, 28);
        tft.text("SD", 65, 104);
      } else {
        root = SD.open("/");
        listFiles(root);
      }
      wait = 1; //wait to prevent constantly rereading the SD card
    } else if (mode == 6) {
      //LDR test mode
      tft.text("LDR Demo", 40, 1);
      tft.text("LDR Value: ", 22, 30);
      tft.setTextSize(3);
      tft.setCursor(50, 50);
      tft.setTextColor(DBROWN, BLACK);
      tft.print(analogRead(A6));
    }
  }

  //15 second idle sleep timer which ensures the PCF interrupt is working if
  //the microcontroller can be brought out of sleep mode with a button press
  if (millis() - idleTimer > 15000) {
    setSleep();
  }
}
