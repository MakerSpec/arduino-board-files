/* This is a simple example sketch for reading multiple inputs
    from the PCF8574 over i2c, using a switch case to determine
    which button was pressed, and displaying the last button
    press to an i2c OLED until the next button is pressed.
    The builtin LED on pin 13 is also toggled every new direction.

    While this is a very simple example depicting how to use the PCF8574
    along side another i2c device and read low inputs, this sketch
    could be modified or adapted to do any number of things...
    Use your imagination!
*/
#include "PCF8574.h"
#include <Wire.h>
#include <SPI.h>
#include <TFT.h>

#define cs   10
#define dc   9
#define rst  8 //12 for v1/v2

TFT tft = TFT(cs, dc, rst);

PCF8574 pcf(0x20); //initialize the PCF8574

int direction = 0;
int lastDirection = 0;
int LEDState = 0;
long a;

void setup() {
  Serial.begin(9600);
  pcf.begin();
  tft.begin();
  tft.fillScreen(BG);
  tft.setTextSize(2);
  tft.setRotation(3);
  tft.stroke(POWDER);
  tft.text("328PTFT Demo", 10, 1);
  tft.line(0, 17, 160, 17);
  tft.setTextColor(TXT, BG);
  //pinMode(13, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(5, HIGH);
 //pcf.write(6, 1);
  a = millis();
}

void loop() {
  for (int i = 0; i < 8; i++) {
    //cycle through all 8 inputs(0-7) to check if any have been pulled low.
    if (pcf.readButton(i) != 1) {
      if (i != 7) {
        pcf.write(i, 1);
        //if one has store the pin number in the direction variable created earlier.
        direction = i;
        a = millis();
      }
    }
  }

  if (direction != lastDirection) {
    tft.setCursor(40, 60);
    switch (direction) {
      case 0:
        //tft.print("RSet    ");
       tft.print("0");
        break;
      case 1:
       // tft.print("Right  ");
        tft.print("1");
        break;
      case 2:
       // tft.print("Down  ");
        tft.print("2");
        break;
      case 3:
        //tft.print("LSet   ");
        tft.print("3");
        break;
      case 4:
       // tft.print("Up     ");
        tft.print("4");
        break;
      case 5:
       // tft.print("Left  ");
        tft.print("5");
        break;
      case 6:
        //tft.print("Center");
        tft.print("6");
        break;
    }
    tft.setCursor(80, 120);
    tft.print(direction);
    //store direction of the last press to compare in the next loop
    lastDirection = direction;
    //print binary value from PCF to serial --
    //0 incidactes which pin has been pulled low
    Serial.print(pcf.read8(), BIN);
    //toggle LED pin on or off, based on the last state, to indicate a button press.
    //           trpcf.write(6, HIGH);
   // pcf.write(6, 1);
    //save the lED pin state in order to output the opposite value on the next keypress.
   // LEDState = !LEDState;
    pcf.toggle(7);
  }

  if(millis() - a > 45000) {
   digitalWrite(5, LOW);
  } else {
   digitalWrite(5, HIGH);
  }
}
