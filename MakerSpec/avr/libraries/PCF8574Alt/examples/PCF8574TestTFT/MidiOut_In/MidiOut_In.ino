void setup() {
  Serial.begin(9600);
  pinMode(0, OUTPUT);
  pinMode(1, INPUT);
}

void loop() {
  digitalWrite(0, HIGH);
  Serial.print(digitalRead(1));
  delay(200);
  digitalWrite(0, LOW);
  Serial.print(digitalRead(1));
  delay(200);
}
