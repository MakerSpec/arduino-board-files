#include <SPI.h>
#include <TFT.h>
#include <RF24.h>

TFT tft = TFT(10, 9, 8);
//TFT tft = TFT(TFT_CS, TFT_DC, TFT_RST);
RF24 nrf(6, 7); //set to 2 & 3 for v2 or 6 & 7 or A1 & A2 for v3

byte addresses[][6] = {"1"};

int drops = 0;
unsigned long lastMillis = 0;
int lastVal = 0;

struct package {
  int id = 2;
  int val = 0;
};

typedef struct package Package;
Package dataout;
package datain;

void setup() {
  pinMode(5, OUTPUT);
  tft.begin();
  tft.fillScreen(BLACK);
  tft.stroke(NUKE);
  nrf.begin();
  nrf.setChannel(115);
  nrf.setPALevel(RF24_PA_MAX);
  nrf.setDataRate(RF24_1MBPS);
  tft.text("Out" , 1, 30);
  tft.text("In", 1, 80);
  tft.text("NRF_PA Test", 10, 0);
  tft.setTextColor(NUKE, BLACK);
  tft.setTextSize(3);

  analogWrite(5, 155);
  nrf.openReadingPipe(1, addresses[0]);
  nrf.startListening();
}

void rfGet() {
  if (nrf.available()) {
    while (nrf.available()) {
      nrf.read(&datain, sizeof(datain));
    }
    tft.setCursor(30, 70);
    //tft.print(char(26));
    tft.print(datain.val);
    //check for lost transmissions since last received
    if(datain.val - 1 > lastVal){
      //increase drops by number of missing transmissions
      drops += (datain.val - lastVal) - 1;
      tft.setCursor(110, 70);
      tft.print(drops);
    }
  }
}

void rfSend() {
  nrf.stopListening();
  nrf.openWritingPipe(addresses[0]);
  nrf.write(&dataout, sizeof(dataout));
  nrf.openReadingPipe(1, addresses[0]);
  tft.setCursor(30, 20);
  tft.print(dataout.val);
  nrf.startListening();
  
}

void loop() {
  if (millis() - lastMillis >= 1000) {
    rfGet();
    dataout.val++;
    rfSend();
    lastMillis = millis();
  }
}
