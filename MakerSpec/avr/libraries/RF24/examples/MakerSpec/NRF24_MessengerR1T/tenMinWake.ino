void tenMinWake() {
  timerFlag = 0;
  idleTimer = millis();
  pcf.write(7, !pcf.read(7));
  timerCnt += 1;
  // rtc.resetTimer();
  // rtc.enableTimer();
  // rtc.setTimer(1, TMR_1MIN, 0);
  Wire.beginTransmission(0x51);//start i2c comm. with PCR8563
  Wire.write(0x00);//jump to register 00h
  Wire.write(B00000000);//set control1 to defaults
  Wire.write(B00010001);//set alarm and timer flags to 0 and interrupts to 1
  Wire.endTransmission();

  Wire.beginTransmission(0x51);
  Wire.write(0x0E);//jump to register 0Eh
  //bit 7 = 1 for timer enable or 0 for disable and bits 1&0 are a combination of 1 or 0 for various clock frequency
  Wire.write(B00000011);
  Wire.endTransmission();
  
  Wire.beginTransmission(0x51);
  Wire.write(0x0E);//jump to register 0Eh
  //bit 7 = 1 for timer enable or 0 for disable and bits 1&0 are a combination of 1 or 0 for various clock frequency
  Wire.write(B10000011); //set timer enabled & source freq = 1Hz -- set to 00000011 when timer not in use
  //timer value bits: 7 = 128 6 = 64 5=32 4=16 3=8 2=4 1=2 0=1
  Wire.write(B00000101); //set timer value register to 10 by setting bits 3 and 1
  Wire.endTransmission();
// rtc.setAlarm(alarmTime, 99, 99, 99); // set 10 minute timer(99 = no value set);
}
