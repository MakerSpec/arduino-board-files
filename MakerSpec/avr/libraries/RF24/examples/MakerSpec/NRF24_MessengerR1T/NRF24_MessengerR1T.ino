/*
   In this example I've tried to cover a very wide range of topics and possible uses for the 328PTFT in a way that works together,
   but can also be broken down and scavanged for the parts you're interrested in! While trying to keep this as simple as possible
   it is still maybe a step above beginner, so if you are unsure of anything feel free to reach out or utilize the resources made
   available by Arduino!

   Please note that some of the libraries used have been modified from their originals and thus will only work with my version.(
   preticularly the TFT library which I've just added additional colors to, and the PCF8563 library which I've added 12hr formatting to).
   All necessary libraries used are available here: gitlab.com/MakerSpec/328ptft

   inside you'll find uses of:
   EEPROM, PCF8574 IO Expander, PCF8563 RTC, NRF24l01 RF transiever, I2C and SPI, ST7735 TFT display, the sleep function,
   button inputs, LEDs, millis(), both external and internal interrupts, char arrays, use of ascii characters, memcpy,
   length(), tone, polling of a 7 buttons, etc.
*/


#include <SPI.h>
#include "RF24.h"
#include <Wire.h>
#include <TFT.h>
#include "PCF8574.h"
#include <Rtc_Pcf8563.h>
#include <EEPROM.h>
#include <avr/sleep.h>

//comment/uncomment the correct pinout for the 5 pos switch used on your board
//Model L - larger nav switch

#define up 5
#define down 3
#define left 2
#define right 1
#define center 6
#define rSet 0
#define lSet 4

/*
  //Model E - low profile nav switch
  #define up 5
  #define down 1
  #define left 6
  #define right 2
  #define center 3
  #define rSet 0
  #define lSet 4
*/

String message;
unsigned long idleTimer = 0;
char messagesOut[4][23];
char messagesIn[4][23];
int letterNum = 65;
int brightness = 110;
int prevMsgLength = 0;
//int alarmTime = 32;
int timerCnt = 0;
int timerFlag = 0;

PCF8574 pcf(0x20);//intialize PCF8574 at i2c address
TFT tft = TFT(10, 9, 8);
Rtc_Pcf8563 rtc;
RF24 nrf(6, 7); //initialize NRF24 -pins D6 & D7 for CE and CSN
byte addresses[][6] = {"0"};

struct package {
  char msg[23];
  int msglength;
};

typedef struct package Package;
Package dataout;
Package datain;

ISR(PCINT2_vect) {
  //PinChangeInterrupt to wake from sleep
  idleTimer = millis();
}

void PCF8563Int() {
  timerFlag = 1;
  detachInterrupt(0);

}

void setup() {
  tft.begin();
  tft.setRotation(3);
  tft.fillScreen(NIGHTSKY);
  tft.stroke(NUKE);
  tft.text("NRF24 Text", 27, 1);
  tft.line(7, 9, 153, 9);
  tft.setCursor(0, 47);
  tft.setTextColor(EPURP, NIGHTSKY);
  //setup PWM pin D5 as output for backlight control
  pinMode(5, OUTPUT);
  //setup D4 as input with internal pullup resistor enabled for the interrupt signal from the PCF8574
  pinMode(4, INPUT_PULLUP);
  //set up D2 as input for the interrupt from the PCF8563 for alarm waking  pinMode(2, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  //get brightness from eeprom
  EEPROM.get(3, brightness);
  if (isnan(brightness)) {
    brightness = 110;
    EEPROM.put(3, brightness);
  }
  //turn on display backlight connected to D5
  analogWrite(5, brightness);
  pcf.begin();
  tenMinWake();
  //setup interrupts for NRF and PCF to allow waking of the processor
  //when a new message or button input is recieved.

  attachInterrupt(digitalPinToInterrupt(2), PCF8563Int, RISING); //using digitalPinToInterrupt(pin number) is now recommended over using the interrupt number

  //setup D4 as interrupt for PCF interrupt pin
  *digitalPinToPCMSK(4) |= bit (digitalPinToPCMSKbit(4));
  PCIFR |= bit (digitalPinToPCICRbit(4));
  PCICR |= bit (digitalPinToPCICRbit(4));
  //  noInterrupts();
  //initialized NRF24
  nrf.begin();
  nrf.setChannel(115);
  //set power level(LOW or MAX and data rate(250KBPS or 1MBPS)
  nrf.setPALevel(RF24_PA_MAX);
  nrf.setDataRate(RF24_2MBPS);
  //alarmTime = (rtc.getMinute(), DEC);
  nrf.openReadingPipe(1, addresses[0]);
  nrf.startListening();

}

void sendMessage() {
  noInterrupts();
  nrf.stopListening();
  //if last message was longer than current message then
  //add trailing spaces to overwrite entire previous message
  while (message.length() < prevMsgLength) {
    message = String(message + " ");
  }
  /*
    the following is my convoluted way for shifting a history of the last 3 messages and
    ensuring if a previous message was longer than the current one it is completely overwritten,
    but without using rectangles to clear sections of the screen and causing flicker.
    -- definitely not neccessary, but looks better */
  //store current message length for next cycle
  prevMsgLength = message.length();
  //shift message history
  memcpy(messagesOut[2], messagesOut[1], 23);
  memcpy(messagesOut[1], messagesOut[0], 23);
  //convert message String to char array
  message.toCharArray(dataout.msg, 23);
  memcpy(messagesOut[0], dataout.msg, 23);
  //print updated history to display
  for (int a = 0; a < 3; a++) {
    tft.setCursor(0, 100 - a * 10); //start with cursor at 0, 100 and move up 10 pixels each loop
    tft.print(char(27)); //print outgoing arrow
    tft.print(messagesOut[(a % 3)]);
  }
  //and finally we actually send our message
  nrf.openWritingPipe(addresses[0]);
  nrf.write(&dataout, sizeof(dataout));
  nrf.openReadingPipe(1, addresses[0]);
  nrf.startListening();
  interrupts();

  //the following lines simply fill the text input line for the next input
  message = "";
  /*
    for (int a = 0; a < 28; a++) {
      message = String(message + " ");
    }
    tft.setCursor(0, 120);
    tft.print(message);
    message.trim();
  */
  //overwrite previous message entry
  tft.stroke(NIGHTSKY);
  tft.fill(NIGHTSKY);
  tft.rect(6, 118, 160, 8);
  tft.setTextColor(EPURP, NIGHTSKY);
}

void getMessage() {
  if (nrf.available()) {
    tone(A3, 722, 182);
    tone(A3, 650, 227);
    tone(A3, 1100, 155);

    //disable interruptions during fetching of message
    noInterrupts();

    while (nrf.available()) {
      nrf.read(&datain, sizeof(datain));
    }
    //shift incomming message history -- oldest out - newest in
    memcpy(messagesIn[2], messagesIn[1], 23);
    memcpy(messagesIn[1], messagesIn[0], 23);
    memcpy(messagesIn[0], datain.msg, 23);
    //reprint message history
    for (int d = 0; d < 3; d++) {
      tft.setCursor(0, 18 + 10 * d);
      tft.print(char(26));
      tft.print(messagesIn[d]);
    }
    //enable interrupts
    interrupts();
  }
}

void loop() {
  int button = 9;//clear button status by setting it to a number above 0-7
  for (int i = 0; i < 8; i++) {
    if (pcf.readButton(i) != 1) {
      button = i;
      pcf.write(i, 1);
    }
  }
  if (button == up) {//up
    letterNum += 1;
    if (letterNum > 90) letterNum = 32;
    if ((letterNum > 33) && (letterNum < 48)) letterNum = 48;
    if ((letterNum > 57) && (letterNum < 63)) letterNum = 63;
  }
  if (button == down) {//down
    letterNum -= 1;
    if (letterNum < 32) {
      letterNum = 90;
    }
    if ((letterNum < 63) && (letterNum > 57)) letterNum = 57;
    if ((letterNum < 48) && (letterNum > 33)) letterNum = 33;
  }
  if (button == right) {//move cursor right/add previous character to message string
    if (message.length() < 23) {
      message = String(message + char(letterNum));
    }
    letterNum = 65;
    tft.setCursor(0, 118);
    tft.print(message);
  }
  //print current character at the end of message input
  tft.setCursor(message.length() * 6, 118);
  tft.print(char(letterNum));

  if (button == left) { // move cursor left
    tft.print(" ");
    if (message.length() > 0) message.remove(message.length() - 1);
    letterNum = 32;
  }
  if (button == center) { //send message on center button press
    sendMessage();
    letterNum = 65;
  }
  if (button == lSet) {
    brightness -= 10;
    if (brightness < 0) brightness = 0;
    analogWrite(5, brightness);
  }
  if (button == rSet) {
    brightness += 10;
    if (brightness > 255) brightness = 255;
    analogWrite(5, brightness);
  }

  //print time in 12:00PM format
  tft.setCursor(118, 1);
  tft.print(rtc.formatTime(RTCC_TIME_12HMM));
  //Jump to the function "getMessage"
  getMessage();
  delay(50);


  tft.setCursor(0, 0);
  tft.print(timerCnt);

  if (timerFlag != 0) {
    tenMinWake();
  }

  //sleep timer - with 30sec of inactivity the display will turn off and MCU will go into sleep mode,
  //only to wake via a reset(pressing the reset button) or an interrupt(button input, alarm, or IRQ event).
  if (millis() - idleTimer > 10000) {
    setSleep();
  }
}
